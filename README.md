# bootstrap-zig ci tarballs

This repository mirrors [Zig Bootstrap](https://github.com/ziglang/zig-bootstrap)
and adds a `.gitlab-ci.yml` file building and publishing the bootstrap tarballs
in an alpine:latest container poweredby my personal VPS.
Through gitlab's CI settings, the CI of the `dasimmet-master` branch is applied
to all refs.

The `master` branch and any tags will be built, compressed into `.tar.xz`
and pushed to the [package registry](https://gitlab.com/dasimmet/zig-bootstrap/-/packages).



## Using it to build zig locally

the tarballs of llvm can be used for building a local zig clone.
This bash function wraps the built zig exe.

```sh
zig-debug(){
    # this function
    # - sets up a local git repository for zig
    # - downloads a prebuilt llvm prefix
    # - uses your hosts zig in PATH to compile a debug version of zig
    # adapt the following lines to your system:
    local ZIG="${ZIG:-$(which zig)}"

    # path to your local repository
    local ZIG_GIT_DIR='~/repos/zig/zig'
    # your host architecture
    local ZIG_BS_TARGET="x86_64-linux-musl-baseline"
    # pick a bootstrap revision from https://gitlab.com/dasimmet/zig-bootstrap/-/packages
    # as close as possible to the version in your zig git repository
    local ZIG_BS_VERSION="0.13.0"

    local ZIG_GIT_DIR="${ZIG_GIT_DIR/#\~/$HOME}"
    local ZIG_LIB_DIR="$ZIG_GIT_DIR/lib"
    local ZIG_PKG_URL="https://gitlab.com/api/v4/projects/dasimmet%2Fzig-bootstrap/packages/generic/zig-bootstrap"
    local PREFIX_URL="$ZIG_PKG_URL/$ZIG_BS_VERSION/$ZIG_BS_TARGET.tar.xz"
    local ZIG_URL="$ZIG_PKG_URL/$ZIG_BS_VERSION/zig-$ZIG_BS_TARGET.tar.xz"
    local PREFIX_PKG_PATH="$ZIG_GIT_DIR/build/search-prefix-$ZIG_BS_VERSION"
    local ZIG_PKG_PATH="$ZIG_GIT_DIR/build/zig-$ZIG_BS_VERSION"

    if [ "$ZIG" = "bootstrap" ]; then
        ZIG="$ZIG_PKG_PATH/zig-$ZIG_BS_TARGET/zig"
    fi
    local SEARCH_PREFIX="$PREFIX_PKG_PATH/$ZIG_BS_TARGET"
    (
        set -e
        if [ ! -d "$ZIG_GIT_DIR/.git" ]; then
            local answer
            read -p "$ZIG_GIT_DIR is not a repository. clone it? (y/n): " answer
            echo "$answer"
            if [ "$answer" != "y" ]; then
                return 2
            else
                (
                    set -ex
                    mkdir -p "$(dirname $ZIG_GIT_DIR)"
                    git clone https://github.com/ziglang/zig.git "$ZIG_GIT_DIR"
                )
            fi
        fi
        (
            set -e
            cd $ZIG_GIT_DIR
            if [ ! -d "$SEARCH_PREFIX" ]; then
                set -x
                rm -rf "$PREFIX_PKG_PATH.installing"
                mkdir -p "$PREFIX_PKG_PATH.installing"
                curl -L "$PREFIX_URL" | \
                    xz -d -T0 | tar -x -C "$PREFIX_PKG_PATH.installing"
                mv "$PREFIX_PKG_PATH.installing" "$PREFIX_PKG_PATH"
            fi

            if [ ! -x "$(which $ZIG)" ]; then
                rm -rf "$ZIG_PKG_PATH.installing"
                mkdir -p "$ZIG_PKG_PATH.installing"
                curl -L "$ZIG_URL" | \
                    xz -d -T0 | tar -x -C "$ZIG_PKG_PATH.installing"
                mv "$ZIG_PKG_PATH.installing" "$ZIG_PKG_PATH"
            fi

            set -x
            # -Dskip-debug=true
            "$ZIG" build \
                -Dskip-release=true \
                -Dskip-libc=true \
                -Dstatic-llvm \
                -Dlog \
                --zig-lib-dir "$ZIG_LIB_DIR" \
                --search-prefix "$SEARCH_PREFIX"
        )
        ZIG_LIB_DIR="$ZIG_LIB_DIR" "$ZIG_GIT_DIR/zig-out/bin/zig" "$@"
    )
}
```

# Mirror job

The default branch `dasimmet-master` runs a mirror job in a weekly pipeline.
